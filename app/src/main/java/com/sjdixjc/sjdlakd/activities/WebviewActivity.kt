package com.sjdixjc.sjdlakd.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.DownloadListener
import android.webkit.WebChromeClient
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebView.WebViewTransport
import android.webkit.WebViewClient
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sjdixjc.sjdlakd.R
import com.sjdixjc.sjdlakd.databinding.ActivityWebviewActivitiesBinding
import com.sjdixjc.sjdlakd.utils.AppConstants
import com.sjdixjc.sjdlakd.utils.webviewutils.JsBridge


class WebviewActivity : AppCompatActivity() {

    lateinit var binding: ActivityWebviewActivitiesBinding
    var newView: WebView? = null
    var context: Context? = null

    //    private LocationReceived locationReceived;
    var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        initView()
        initWebView()
    }

    private fun initWebView() {
        WebView.setWebContentsDebuggingEnabled(true)

        binding.webview.apply {
            var userAgent: String = settings.userAgentString
            userAgent = "$userAgent[NATIVE/WV]"
            settings.userAgentString = userAgent
            settings.setSupportZoom(false)
            enableWebSettingsOn(this)
            loadUrl(AppConstants.URL)
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun enableWebSettingsOn(webView: WebView) {
        webView.apply {
            addJavascriptInterface(
                JsBridge(
                ), "jsBridge"
            )
            settings.apply {
                javaScriptEnabled = true
                mediaPlaybackRequiresUserGesture = false
                databaseEnabled = true
                allowFileAccess = true
                allowContentAccess = true
                domStorageEnabled = true
                setSupportMultipleWindows(true)
                javaScriptCanOpenWindowsAutomatically = true
            }
            webViewClient = PwaWebViewClient()
            webChromeClient = object : WebChromeClient() {
                override fun onCreateWindow(
                    view: WebView?,
                    isDialog: Boolean,
                    isUserGesture: Boolean,
                    resultMsg: Message?
                ): Boolean {
                    newView = WebView(context)
                    enableWebSettingsOn(newView!!)
                    // Create dynamically a new view
                    // Create dynamically a new view
                    newView!!.layoutParams =
                        RelativeLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                    addView(newView)
                    val transport = resultMsg!!.obj as WebViewTransport
                    transport.webView = newView
                    newView!!.visibility = View.INVISIBLE
                    resultMsg.sendToTarget()
                    return true
                }
            }

            setDownloadListener(DownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
                Log.d("download", url)
                val fileExtension = url.substring(url.lastIndexOf(".") + 1, url.length)
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            })

        }
    }

    override fun onBackPressed() {
        if (binding.webview != null) {
            if (binding.webview.canGoBack()) {
                binding.webview.goBack()
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    return
                }
                doubleBackToExitPressedOnce = true
                Toast.makeText(
                    this, R.string.press_back_again_to_exit,
                    Toast.LENGTH_SHORT
                ).show()
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)


            }
        } else {
            super.onBackPressed()
        }

    }

    private fun initView() {
        binding = ActivityWebviewActivitiesBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


    private class PwaWebViewClient : WebViewClient() {
        override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest,
            error: WebResourceError
        ) {
        }

        override fun onReceivedHttpError(
            view: WebView,
            request: WebResourceRequest,
            errorResponse: WebResourceResponse
        ) {
        }

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView, url: String) {
            Log.d("download", "onPageFinished $url")

        }

        override fun onPageCommitVisible(view: WebView, url: String) {
            super.onPageCommitVisible(view, url)

        }

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            Log.d("download", "shouldOverrideUrlLoading ${view.url}")
            var url: String? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                url = request.url.toString()
            }
            if (url == null) {
                return super.shouldOverrideUrlLoading(view, request)
            }
            if (url.endsWith(".apk")
            ) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                view.context.startActivity(intent)
                return true
            }
            return false
        }


    }

}