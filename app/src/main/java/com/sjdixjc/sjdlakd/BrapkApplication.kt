package com.sjdixjc.sjdlakd

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerLib
import com.sjdixjc.sjdlakd.utils.AppConstants

class BrapkApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        mInstance = this
        Log.d("AF","initialised")

        // Initializing Appsflyer SDK
        AppsFlyerLib.getInstance().init(AppConstants.AppsFlyerDevKey, null, this)
        AppsFlyerLib.getInstance().start(this)
        AppsFlyerLib.getInstance().setDebugLog(true)
    }

    companion object {
        lateinit var mInstance: BrapkApplication

        @JvmStatic
        fun getInstance(): BrapkApplication {
            if (mInstance == null) {
                mInstance = BrapkApplication()
            }
            return mInstance
        }
    }


    fun logsAppFlyerEvents(eventName: String?, eventsJson: String?) {
        Log.d("AF","logsAppFlyerEvents")

        val eventMap = HashMap<String, Any>()
        eventName?.let {
            eventsJson?.let {
                eventMap[eventName] = it
                AppsFlyerLib.getInstance().logEvent(this, eventName, eventMap)
            }

        }
    }


}