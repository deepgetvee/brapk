package com.sjdixjc.sjdlakd.utils.webviewutils

import android.util.Log
import android.webkit.JavascriptInterface
import com.sjdixjc.sjdlakd.BrapkApplication
import com.sjdixjc.sjdlakd.utils.Apputils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class JsBridge {

    @JavascriptInterface
    fun pushMessage(eventName: String, params: String) {
        Log.d("BprakJsBridge", "eventName: $eventName ")
        if (Apputils.isJSONValid(params)) {
            processWebSdkEvents(eventName, params)
        }
    }

    private fun processWebSdkEvents(eventName: String, eventMapJson: String) {
        CoroutineScope(Dispatchers.Main).launch {
            BrapkApplication.getInstance().logsAppFlyerEvents(eventName, eventMapJson)
        }
    }
}