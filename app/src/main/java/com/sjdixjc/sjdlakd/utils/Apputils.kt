package com.sjdixjc.sjdlakd.utils

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class Apputils {
    companion object {
        @JvmStatic
        fun isJSONValid(test: String?): Boolean {
            try {
                JSONObject(test)
            } catch (ex: JSONException) {
                try {
                    JSONArray(test)
                } catch (ex1: JSONException) {
                    return false
                }
            }
            return true
        }
    }

}